import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-firstscreen',
  templateUrl: './firstscreen.component.html',
  styleUrls: ['./firstscreen.component.css']
})
export class FirstscreenComponent implements OnInit {

  public form: NgForm;
  public players: FormGroup;


  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
  ) {
    this.players = formBuilder.group({
      player1: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
    });

  }

  ngOnInit() {

  }




  onSubmit(form: NgForm) {
    if (form.form.status === 'VALID') {
      sessionStorage.setItem('player', form.value.player1);
      this.router.navigate(['/secondscreen']);
    } else {
      alert(`write correct name please!`);
    }
  }
}
