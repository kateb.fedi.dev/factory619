import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PlayerModel } from 'src/app/model/player.model';
import { FirebaseService } from 'src/app/services/firebase.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-secondscreen',
  templateUrl: './secondscreen.component.html',
  styleUrls: ['./secondscreen.component.css']
})
export class SecondscreenComponent implements OnInit {

  games: Array<PlayerModel>;
  player: string;
  game: PlayerModel = new PlayerModel(null, null, null, null, null, null, null);
  items: Observable<any[]>;

  constructor(private db: AngularFireDatabase,
              private firebaseService: FirebaseService,
              private router: Router,
  ) {

  }


  ngOnInit() {
    this.getGames();
    this.player = sessionStorage.getItem('player');

  }


  getGames() {
    this.firebaseService.getGames().subscribe(dataUsers => {
      this.games = dataUsers;

    });
  }


  new() {
    this.game.player1 = this.player;
    this.game.turn = this.player;
    this.firebaseService.saveGame(this.game).then(docref => {
      const id = (docref) ? (docref as any).id : 'void';
      sessionStorage.setItem('IdGame', id);
      sessionStorage.setItem('role', '1');
      this.router.navigate(['/thirdscreen']);

    });
  }



  join( id: string) {
    this.firebaseService.getGame(id).subscribe(
      dataUser => {
        this.game.id = dataUser.id;
        this.game.player1 = dataUser.player1;
        this.game.player2 = this.player;
        this.game.scorep1 = '0';
        this.game.scorep2 = '0';
        this.game.turn = dataUser.turn;
        this.game.winner = dataUser.winner;

        const g = [{
          id: this.game.id,
          player1: this.game.player1,
          player2: this.game.player2,
          scorep1: this.game.scorep1,
          scorep2: this.game.scorep2,
          turn: this.game.turn,
          winner: this.game.winner,
        }];
        sessionStorage.setItem('IdGame', this.game.id);
        sessionStorage.setItem('game', JSON.stringify(g));
        sessionStorage.setItem('role', '2');
        this.firebaseService.updateGame(this.game).then(() => {
          this.router.navigate(['/thirdscreen']);

        });
      }
    );
  }
}
